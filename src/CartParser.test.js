import CartParser from './CartParser';
import fs from 'fs';
import path from 'path';

jest.mock('fs');
const pureFs = jest.requireActual('fs');

describe('CartParser - unit tests', () => {
    const cartParser = new CartParser;
    const mockSchemaColumns = [
        {
            name: 'Foo',
            key: 'foo',
            type: cartParser.ColumnType.STRING
        },
        {
            name: 'Bar',
            key: 'bar',
            type: cartParser.ColumnType.NUMBER_POSITIVE
        }
    ];
    cartParser.schema = {columns: mockSchemaColumns};

    let fsSpyOn;

    describe('parse', () => {
        it('should throw error if data by given path is invalid', () => {
            fs.readFileSync
                .mockReturnValueOnce('Foo, Baz \n value, 1')
                .mockReturnValueOnce('Foo, Bar \n value')
                .mockReturnValueOnce('Foo, Bar \n , 2')
                .mockReturnValueOnce('Foo, Bar \n value, value2');
    
            const errorFunc = () => cartParser.parse('mocked/example.csv');
    
            expect(errorFunc).toThrow('Validation failed!');
            expect(errorFunc).toThrow('Validation failed!');
            expect(errorFunc).toThrow('Validation failed!');
            expect(errorFunc).toThrow('Validation failed!');
        });

        it('should return JSON object', () => {
            fs.readFileSync.mockReturnValueOnce('Foo, Bar, \n test, 1');
            const result = cartParser.parse('mocked/example.csv');
            expect(result).toBeInstanceOf(Object);
            expect(result).not.toBeInstanceOf(Array);
        });
    });

    describe('validate', () => {
        it('should return error if content headers don\'t correspond to schema headers', () => {
            const result = cartParser.validate('Foo, Baz \n value, 1');
            expect(result).toBeInstanceOf(Array);
            expect(result).not.toHaveLength(0);
            expect(result[0].type).toBe('header');
        });

        it('should return error if schema cells\' count is not equal to given row cells count', () => {
            const result = cartParser.validate('Foo, Bar \n value');
            expect(result).toBeInstanceOf(Array);
            expect(result).not.toHaveLength(0);
            expect(result[0].type).toBe('row');
        });

        it('should return error if given cells contain empty string instead of text', () => {
            const result = cartParser.validate('Foo, Bar \n , 2');
            expect(result).toBeInstanceOf(Array);
            expect(result).not.toHaveLength(0);
            expect(result[0].type).toBe('cell');
        });

        it('should return error if given cell content should be positive number, but it\'s not', () => {
            const result1 = cartParser.validate('Foo, Bar \n value, value2');
            const result2 = cartParser.validate('Foo, Bar \n value, -2');

            expect(result1).toBeInstanceOf(Array);
            expect(result1).not.toHaveLength(0);
            expect(result1[0].type).toBe('cell');

            expect(result2).toBeInstanceOf(Array);
            expect(result2).not.toHaveLength(0);
            expect(result2[0].type).toBe('cell');
        });

        it('should return empty array if csv-content is valid', () => {
            const csv = `
                Foo,            Bar
                valid,          1
                also valid,     2
                again valid,    3
            `.trim();
            const result = cartParser.validate(csv);
            expect(result).toBeInstanceOf(Array);
            expect(result).toHaveLength(0);
        });
    });

    describe('calcTotal', () => {
        it('should calculate item price', () => {
            const mock1 = [
                { name: 'test', price: 4, quantity: 2 },
                { name: 'example', price: 12, quantity: 1 },
            ];
            const mock2 = [
                { name: 'test', price: 1, quantity: 2 }
            ];
            const mock3 = [
                { name: 'test', price: 1.499, quantity: 3 },
                { name: 'example', price: 2, quantity: 5 }
            ];
    
            const result1 = cartParser.calcTotal(mock1);
            const result2 = cartParser.calcTotal(mock2);
            const result3 = cartParser.calcTotal(mock3);
            const result4 = cartParser.calcTotal([]);
    
            expect(result1).toBe(20);
            expect(result2).toBe(2);
            expect(result3).toBeCloseTo(14.5);
            expect(result4).toBe(0);
        });
    });

    describe('parseLine', () => {
        it('should parse csv line', () => {
            const parsedLine = cartParser.parseLine('test, 1');
            expect(parsedLine).toMatchObject({ foo: 'test', bar: 1 });
        });
    });

    describe('createError', () => {
        it('should return value', () => {
            const result = cartParser.createError('error type', 0, 0, 'error message');
            expect(result).toBeTruthy();
        })
    });

    describe('readFile', () => {
        it('should return value of string type', () => {
            fs.readFileSync.mockImplementationOnce((...args) => pureFs.readFileSync(...args));
            const csvPath = path.join(__dirname, '../', 'samples', 'cart.csv');
            const result = cartParser.readFile(csvPath);

            expect(typeof result === 'string').toBeTruthy();
        })
    });
});

describe('CartParser - integration test', () => {
    const cartParser = new CartParser;
    fs.readFileSync.mockImplementationOnce((...args) => pureFs.readFileSync(...args));
    describe('cart.json', () => {
        it('should be equal to parsed card.csv in sample', () => {
            const expectedResult = require('../samples/cart.json');
            const csvPath = path.join(__dirname, '../', 'samples', 'cart.csv');
            const result = cartParser.parse(csvPath);

            result.items.forEach(item => delete item.id);
            expectedResult.items.forEach(item => delete item.id);

            expect(result).toEqual(expectedResult);
        })
    })
});